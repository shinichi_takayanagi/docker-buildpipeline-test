## What's this?
bitbucket pipeline test for docker (AWS ECR)

## Settings
### Environment variables
Add the following environemnts variables to [Repository variables](https://ja.confluence.atlassian.com/bitbucket/notifications-for-bitbucket-pipelines-857053284.html).

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

### Slack Notification
[Chat notifications](https://bitbucket.org/shinichi_takayanagi/docker-buildpipeline-test/admin/addon/admin/bitbucket-chats-integration/repo-config-admin)

https://ja.confluence.atlassian.com/bitbucket/notifications-for-bitbucket-pipelines-857053284.html
